using System;
using Deaven.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.RpgCharacter
{
    [Serializable]
    public class Character : MonoBehaviour
    {
        public CharacterClass activeClass;

        [HideInInspector] public CharacterClass runtimeClass;

        private int _level = 1;
        private StatGroup _statModifiers = new StatGroup();
        private StatGroup _characterStats = new StatGroup();

        public event Action StatChanged;
        public event Action LevelChanged;
        
        [ShowInInspector, ReadOnly]
        public int Level
        {
            get => _level;
            set => _level = value;
        }

        [ShowInInspector, ReadOnly]
        public virtual int Damage
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.damage;
                int runtimeValue = runtimeClass.statGroup.Strength * 5;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public virtual int MaxHealth
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.maxHealth;
                int runtimeValue = runtimeClass.statGroup.Endurance * 10;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public virtual int Defense
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.defense;
                int runtimeValue = runtimeClass.statGroup.Endurance * 5;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly] public StatGroup CharacterStats => _characterStats;

        [ShowInInspector, ReadOnly] public StatGroup StatModifiers => _statModifiers;

        private void Start()
        {
            Init();
        }

        [Button, FoldoutGroup("Actions")]
        public virtual void Init()
        {
            Level = 1;

            runtimeClass = ScriptableObject.CreateInstance<CharacterClass>();
            runtimeClass.damage = activeClass.damage;
            runtimeClass.defense = activeClass.defense;
            runtimeClass.maxHealth = activeClass.maxHealth;

            SetStatsPerLevel();

            _characterStats = runtimeClass.statGroup;

            _statModifiers.stats = new StatList();
            SetClassStats();

            StatChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public virtual void ChangeClassStats(CharacterClass newClass)
        {
            SubtractStatGroup(activeClass.statGroup);

            activeClass = newClass;
            
            SetStatsPerLevel();
            SetClassStats();
        }
        
        [Button, FoldoutGroup("Actions")]
        public virtual void SetLevel(int value)
        {
            Level = value <= 1 ? 1 : value;

            _characterStats.Strength =
                (Level - 1) * runtimeClass.strengthPerLevel + activeClass.statGroup.Strength;
            _characterStats.Dexterity =
                (Level - 1) * runtimeClass.dexterityPerLevel + activeClass.statGroup.Dexterity;
            _characterStats.Intelligence =
                (Level - 1) * runtimeClass.intelligencePerLevel + activeClass.statGroup.Intelligence;
            _characterStats.Endurance =
                (Level - 1) * runtimeClass.endurancePerLevel + activeClass.statGroup.Endurance;

            _characterStats.stats.Add(_statModifiers.stats);

            StatChanged?.Invoke();
            LevelChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public virtual void LevelUp()
        {
            Level++;

            _characterStats.Strength += runtimeClass.strengthPerLevel;
            _characterStats.Dexterity += runtimeClass.dexterityPerLevel;
            _characterStats.Intelligence += runtimeClass.intelligencePerLevel;
            _characterStats.Endurance += runtimeClass.endurancePerLevel;

            StatChanged?.Invoke();
            LevelChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public virtual void LevelDown()
        {
            if (Level - 1 < 1)
                return;

            Level--;

            _characterStats.Strength -= runtimeClass.strengthPerLevel;
            _characterStats.Dexterity -= runtimeClass.dexterityPerLevel;
            _characterStats.Intelligence -= runtimeClass.intelligencePerLevel;
            _characterStats.Endurance -= runtimeClass.endurancePerLevel;

            StatChanged?.Invoke();
            LevelChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public void IncreaseStat(StatType type, int value)
        {
            _statModifiers.stats[type] += value;
            _characterStats.stats[type] += value;

            StatChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public void DecreaseStat(StatType type, int value)
        {
            if (_statModifiers.stats[type] - value < 0)
            {
                _characterStats.stats[type] -= _statModifiers.stats[type];
                _statModifiers.stats[type] = 0;
                return;
            }

            _statModifiers.stats[type] -= value;
            _characterStats.stats[type] -= value;

            StatChanged?.Invoke();
        }

        [Button, FoldoutGroup("Actions")]
        public void RemoveStatModifiers()
        {
            _characterStats.stats.Subtract(_statModifiers.stats);
            _statModifiers.stats = new StatList();

            StatChanged?.Invoke();
        }

        public void AddStatGroup(StatGroup group)
        {
            _statModifiers.stats.Add(group.stats);
            _characterStats.stats.Add(group.stats);
            
            StatChanged?.Invoke();
        }

        public void SubtractStatGroup(StatGroup group)
        {
            _statModifiers.stats.Subtract(group.stats);
            _characterStats.stats.Subtract(group.stats);
            
            StatChanged?.Invoke();
        }
        
        private void SetClassStats()
        {
            AddStatGroup(activeClass.statGroup);
        }

        private void SetStatsPerLevel()
        {
            runtimeClass.strengthPerLevel = activeClass.strengthPerLevel;
            runtimeClass.dexterityPerLevel = activeClass.dexterityPerLevel;
            runtimeClass.intelligencePerLevel = activeClass.intelligencePerLevel;
            runtimeClass.endurancePerLevel = activeClass.endurancePerLevel;
        }
    }
}