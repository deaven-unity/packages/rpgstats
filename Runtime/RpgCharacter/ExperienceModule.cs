using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.RpgCharacter
{
    [RequireComponent(typeof(Character))]
    public class ExperienceModule : MonoBehaviour
    {
        [SerializeField] private ExperienceTable expTable;
        [SerializeField, ReadOnly] private int totalExp;

        private Character _character;

        public event Action ExperienceChanged;

        public int TotalExp
        {
            get => totalExp;
            set
            {
                if (IsMaxLevel()) 
                    return;
                
                totalExp = value;
                ExperienceChanged?.Invoke();
                    
                if (totalExp >= expTable.Table[_character.Level - 1])
                    _character.LevelUp();
            }
        }

        [ShowInInspector, ReadOnly]
        public int ExpNeeded
        {
            get
            {
                if (!_character || IsMaxLevel())
                    return 0;
                
                return expTable.Table[_character.Level - 1] - totalExp;
            }
        }

        private void Awake()
        {
            _character = GetComponent<Character>();
        }

        private void Start()
        {
            totalExp = 0;
        }

        private void OnEnable()
        {
            _character.LevelChanged += () =>
            {
                if (_character.Level >= expTable.Table.Count)
                {
                    totalExp = expTable.Table[^1];
                    return;
                }
                
                if (expTable.Table[_character.Level - 1] >= totalExp)
                    totalExp = expTable.Table[_character.Level - 1];
            };
        }

        private bool IsMaxLevel()
        {
            return _character.Level - 1 >= expTable.Table.Count;
        }

        [Button, HideInEditorMode]
        public void AddExperience(int value)
        {
            TotalExp += value;
        }

        [Button, HideInEditorMode]
        public void SubtractExperience(int value)
        {
            TotalExp -= value;
        }

        [Button]
        public void ResetExperience()
        {
            TotalExp = 0;
        }
    }
}