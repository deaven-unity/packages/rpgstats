using Deaven.Stats;
using UnityEngine;

namespace Deaven.RpgCharacter
{
    [CreateAssetMenu(fileName = "CharacterClass", menuName = "RpgStats/CharacterClass", order = 0)]
    public class CharacterClass : ScriptableObject
    {
        public int damage = 10;
        public int defense = 10;
        public int maxHealth = 100;
        
        public int strengthPerLevel = 2;
        public int dexterityPerLevel = 2;
        public int intelligencePerLevel = 2;
        public int endurancePerLevel = 2;
        
        public StatGroup statGroup = new StatGroup();
    }
}