using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.RpgCharacter
{
    [CreateAssetMenu(fileName = "ExperienceTable", menuName = "RpgStats/ExperienceTable", order = 0)]
    public class ExperienceTable : ScriptableObject
    {
        [ListDrawerSettings(ShowIndexLabels = true, Expanded = true, NumberOfItemsPerPage = 25, ShowPaging = true)]
        [ShowInInspector]
        private List<int> _table;
        
        [ShowInInspector, ReadOnly, PropertyOrder(-1)]
        public int TotalExperience
        {
            get
            {
                int sum = 0;

                foreach (int i in _table)
                    sum += i;

                return sum;
            }
        }

        public List<int> Table => _table;
    }
}