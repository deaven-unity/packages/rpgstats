using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Deaven.Stats
{
    [Serializable]
    public class StatList : IEnumerable<StatValue>
    {
        [SerializeField] private List<StatValue> _stats = new List<StatValue>();

        public event Action StatChanged;

        public int Count
        {
            get { return _stats.Count; }
        }

        public StatValue this[int index]
        {
            get { return _stats[index]; }
            set { _stats[index] = value; }
        }

        public int this[StatType type]
        {
            get
            {
                for (int i = 0; i < _stats.Count; i++)
                {
                    if (_stats[i].Type == type)
                    {
                        return _stats[i].Value;
                    }
                }

                return 0;
            }
            set
            {
                for (int i = 0; i < _stats.Count; i++)
                {
                    if (_stats[i].Type == type)
                    {
                        var val = _stats[i];
                        val.Value = value;
                        _stats[i] = val;
                        return;
                    }
                }

                _stats.Add(new StatValue(type, value));
            }
        }

        public void Add(StatList statList)
        {
            foreach (StatValue stat in statList)
                this[stat.Type] += stat.Value;
        }

        public void Subtract(StatList statList)
        {
            foreach (StatValue stat in statList)
            {
                if (this[stat.Type] - stat.Value < 0)
                    continue;

                this[stat.Type] -= stat.Value;
            }
        }

        public IEnumerator<StatValue> GetEnumerator()
        {
            foreach (StatValue stat in _stats)
                yield return stat;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}