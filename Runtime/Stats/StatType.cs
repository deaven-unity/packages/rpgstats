namespace Deaven.Stats
{
    public enum StatType
    {
        Strength,
        Dexterity,
        Intelligence,
        Endurance
    }
}