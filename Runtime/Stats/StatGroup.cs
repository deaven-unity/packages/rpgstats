using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.Stats
{
    [Serializable]
    public class StatGroup
    {
        public StatGroup()
        {
        }
        
        public StatGroup(StatGroup stats)
        {
            Strength = stats.Strength;
            Dexterity = stats.Dexterity;
            Intelligence = stats.Intelligence;
            Endurance = stats.Endurance;
        }

        [HideInInspector]
        public StatList stats = new StatList();

        [ShowInInspector]
        public int Power
        {
            get => Strength + Dexterity + Intelligence + Endurance;
        }

        [ShowInInspector]
        public int Average
        {
            get => Power / 4;
        }

        [ShowInInspector]
        public int Strength
        {
            get => stats[StatType.Strength];
            set => stats[StatType.Strength] = value;
        }

        [ShowInInspector]
        public int Dexterity
        {
            get => stats[StatType.Dexterity];
            set => stats[StatType.Dexterity] = value;
        }

        [ShowInInspector]
        public int Intelligence
        {
            get => stats[StatType.Intelligence];
            set => stats[StatType.Intelligence] = value;
        }

        [ShowInInspector]
        public int Endurance
        {
            get => stats[StatType.Endurance];
            set => stats[StatType.Endurance] = value;
        }
    }
}