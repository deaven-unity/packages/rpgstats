using System;

namespace Deaven.Stats
{
    [Serializable]
    public struct StatValue : IEquatable<StatValue>
    {
        public StatType Type;
        public int Value;

        public StatValue(StatType type, int value)
        {
            Type = type;
            Value = value;
        }

        public StatValue(StatType type)
        {
            Type = type;
            Value = 0;
        }

        public bool Equals(StatValue other)
        {
            return Type == other.Type;
        }
    }
}
