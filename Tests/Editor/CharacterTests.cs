using Deaven.RpgCharacter;
using Deaven.Stats;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Editor
{
    public class CharacterTests
    {
        [Test]
        public void InitializeCharacterWithDefaultClass()
        {
            Character cc = GetCharacter();
            cc.Init();
            
            Assert.AreEqual(60, cc.Damage);
            Assert.AreEqual(10,cc.Defense);
            Assert.AreEqual(100,cc.MaxHealth);
            Assert.AreEqual(10,cc.runtimeClass.statGroup.Strength);
            Assert.AreEqual(0,cc.runtimeClass.statGroup.Dexterity);
            Assert.AreEqual(0,cc.runtimeClass.statGroup.Intelligence);
            Assert.AreEqual(0,cc.runtimeClass.statGroup.Endurance);
        }
        
        [Test]
        public void NewCharacterIsInitializedWithLevelOne()
        {
            Character cc = GetCharacter();
            
            Assert.AreEqual(1, cc.Level);
        }
        
        [Test]
        public void IncreasesLevelByOne()
        {
            Character cc = GetCharacter();
            cc.Init();
            cc.LevelUp();

            Assert.AreEqual(2,cc.Level);
        }

        [Test]
        public void IncreasingLevelIncreasesStrengthStat()
        {
            Character cc = GetCharacter();
            cc.Init();
            
            Assert.AreEqual(10, cc.runtimeClass.statGroup.Strength);
            cc.LevelUp();
            Assert.AreEqual(12, cc.runtimeClass.statGroup.Strength);
        }
        
        [Test]
        public void DecreaseLevelByOne()
        {
            Character cc = GetCharacter();
            cc.Init();
            cc.LevelUp();
            cc.LevelDown();
            
            Assert.AreEqual(1, cc.Level);
        }

        [Test]
        public void CantDecreasesLevelBeneathOne()
        {
            Character cc = GetCharacter();
            cc.LevelDown();
            
            Assert.AreEqual(1, cc.Level);
        }

        [Test]
        public void SetsASpecificLevel()
        {
            Character cc = GetCharacter();
            cc.Init();
            cc.SetLevel(10);
            
            Assert.AreEqual(10, cc.Level);
        }

        private Character GetCharacter()
        {
            GameObject go = new GameObject();
            CharacterClass defaultClass = ScriptableObject.CreateInstance<CharacterClass>();
            go.AddComponent<Character>();

            Character character = go.GetComponent<Character>();
            
            defaultClass.damage = 10;
            defaultClass.defense = 10;
            defaultClass.maxHealth = 100;

            defaultClass.strengthPerLevel = 2;
            defaultClass.dexterityPerLevel = 2;
            defaultClass.intelligencePerLevel = 2;
            defaultClass.endurancePerLevel = 2;

            defaultClass.statGroup = new StatGroup();
            defaultClass.statGroup.Strength = 10;

            character.activeClass = defaultClass;
            
            return go.GetComponent<Character>();
        }
    }
}