using Deaven.Stats;
using NUnit.Framework;

namespace Tests.Editor
{
    public class StatValueTests
    {
        [Test]
        public void CreateStrengthStatValueWithoutValue()
        {
            StatValue statValue = new StatValue(StatType.Strength);
            
            Assert.AreEqual(StatType.Strength, statValue.Type);
        }
        
        [Test]
        public void CreateStrengthStatValueWithValue()
        {
            StatValue statValue = new StatValue(StatType.Strength, 10);
            
            Assert.AreEqual(StatType.Strength, statValue.Type);
        }

        [Test]
        public void CompareTwoSameTypeStatValues()
        {
            StatValue statValue1 = new StatValue(StatType.Strength, 10);
            StatValue statValue2 = new StatValue(StatType.Strength, 15);
            
            Assert.AreEqual(true, statValue1.Equals(statValue2));
        }
        
        [Test]
        public void CompareTwoDifferentTypeStatValues()
        {
            StatValue statValue1 = new StatValue(StatType.Strength, 10);
            StatValue statValue2 = new StatValue(StatType.Intelligence, 15);
            
            Assert.AreEqual(false, statValue1.Equals(statValue2));
        }
    }
}