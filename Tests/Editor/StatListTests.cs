using Deaven.Stats;
using NUnit.Framework;

namespace Tests.Editor
{
    public class StatListTests
    {
        [Test]
        public void CreateEmptyStatList()
        {
            StatList statList = new StatList();

            Assert.AreEqual(0, statList.Count);
        }
        
        [Test]
        public void AccessingNoneExistingTypeCreatesNewOne()
        {
            StatList statList = new StatList();
            
            statList[StatType.Strength] = 10;
            Assert.AreEqual(1, statList.Count);
        }
        
        [Test]
        public void SetValueOfStatType()
        {
            StatList statList = new StatList();
            
            statList[StatType.Strength] = 10;
            Assert.AreEqual(10, statList[StatType.Strength]);
        }
    }
}