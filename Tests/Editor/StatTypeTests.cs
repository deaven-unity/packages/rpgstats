using System;
using Deaven.Stats;
using NUnit.Framework;

namespace Tests.Editor
{
    public class StatTypeTests
    {
        [Test]
        public void StrengthTypeExists()
        {
            Assert.AreEqual(true, Enum.IsDefined(typeof(StatType), "Strength"));
        }
        
        [Test]
        public void DexterityTypeExists()
        {
            Assert.AreEqual(true, Enum.IsDefined(typeof(StatType), "Dexterity"));
        }
        
        [Test]
        public void IntelligenceTypeExists()
        {
            Assert.AreEqual(true, Enum.IsDefined(typeof(StatType), "Intelligence"));
        }
        
        [Test]
        public void EnduranceTypeExists()
        {
            Assert.AreEqual(true, Enum.IsDefined(typeof(StatType), "Endurance"));
        }
    }
}