# Unity: Rpg Stats

## How to install

* Open the Package Manager
* Plus-Icon: Add package from git URL...
* Use the HTTPS address found in "Clone"

# Usage

## Creating new Character Classes

* Project Folder > Right click > Create > RpgStats > CharacterClass

## Assign Character Class to Character

* Assign Character MonoBehaviour and select your created CharacterClass

# Extending

You can change the calculations for each Character Properties, Methods (e.g. Damage, Defense, LevelUp)

1. Create your custom character MonoBehaviour and extend Character
2. Overwrite your desired Property/Method

```cs
public override int Damage
{
    get
    {
        if (runtimeClass == null)
            return 0;

        int modification = 10;

        int defaultValue = runtimeClass.damage;
        int runtimeValue = runtimeClass.stats.Strength * 5 + modification;

        int value = defaultValue + runtimeValue;

        return value;
    }
}
```

# Content

## Default Stat Types

Currently not extendable

* Strength
* Dexterity
* Intelligence
* Endurance